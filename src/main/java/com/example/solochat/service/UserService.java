package com.example.solochat.service;

import java.util.List;

import com.example.solochat.model.ChatMessage;
import com.example.solochat.model.Users;
import com.example.solochat.requestDto.RegisterDto;

public interface UserService {

	void registerUser(RegisterDto registerDto);

	List<Users> getUsersList(int offset,int pageNo);

	List<ChatMessage> getChats(long fromUserId, long toUserId,int offset,int pageNo);

}
