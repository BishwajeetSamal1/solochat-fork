package com.example.solochat.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.example.solochat.dao.ChatMessageRepository;
import com.example.solochat.dao.UsersRepository;
import com.example.solochat.model.ChatMessage;
import com.example.solochat.model.Users;
import com.example.solochat.requestDto.RegisterDto;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UsersRepository usersRepository;
	@Autowired
	ChatMessageRepository chatMessageRepository;
	
	@Override
	public void registerUser(RegisterDto registerDto) {
		Users u = new Users();
		u.setId(registerDto.getId());
		u.setUserName(registerDto.getUserName());
		usersRepository.save(u);
		
	}

	@Override
	public List<Users> getUsersList(int offset,int pageNo) {
		List<Users> usersList =  usersRepository.findAll(PageRequest.of((offset-1), pageNo)).toList();
		return usersList;
	}

	@Override
	public List<ChatMessage> getChats(long fromUserId, long toUserId,int offset,int pageNo) {
		Optional<Users> fromUser = usersRepository.findById(fromUserId);
		Optional<Users> toUser = usersRepository.findById(toUserId);
		List<ChatMessage> chatMessage = chatMessageRepository.findByFromUserAndToUserOrderByCreatedAt(fromUser.get(),toUser.get());
	
		return chatMessage;
	}

}
