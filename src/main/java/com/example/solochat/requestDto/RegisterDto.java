package com.example.solochat.requestDto;

import lombok.Data;

@Data
public class RegisterDto {
	private long id;
	private String userName;
}
