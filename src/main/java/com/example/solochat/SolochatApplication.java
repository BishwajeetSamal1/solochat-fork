package com.example.solochat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SolochatApplication {

	public static void main(String[] args) {
		SpringApplication.run(SolochatApplication.class, args);
	}

}
