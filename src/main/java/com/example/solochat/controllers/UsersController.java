package com.example.solochat.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.solochat.model.ChatMessage;
import com.example.solochat.model.Users;
import com.example.solochat.requestDto.RegisterDto;
import com.example.solochat.service.UserService;
@RequestMapping("/user")
@RestController
public class UsersController {
	
	@Autowired
	UserService userService;
		
	@PostMapping("/registeruser")
	public Users registerUser(@RequestBody RegisterDto registerDto)
	{
		
		userService.registerUser(registerDto);
		return null;
	}
	
	@GetMapping("/getuserslist/{offset}/{pageNo}")
	public List<Users> getUsersList(@PathVariable("offset") int offset,@PathVariable("pageNo") int pageSize)
	{
		return userService.getUsersList(offset,pageSize);
	}
	
	@GetMapping("/getchats/{fromUserId}/{toUserId}/{offset}/{pageNo}")
	public List<ChatMessage> getChats(@PathVariable("fromUserId") long fromUserId, @PathVariable("toUserId") long toUserId,@PathVariable("offset") int offset,@PathVariable("pageNo") int pageSize)
	{
		return userService.getChats(fromUserId,toUserId,offset,pageSize);
	}
	
	
	
}
