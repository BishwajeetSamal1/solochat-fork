package com.example.solochat.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

import com.example.solochat.dto.ChatRequestDto;
import com.example.solochat.model.ChatMessage;
import com.example.solochat.services.ChatMessageService;

@Controller
public class ChatController {
	
	
	@Autowired
	ChatMessageService chatMessageService;
	
	@MessageMapping("{fromUserId}/sendMsg")
	public ResponseEntity<ChatMessage> sendMesssge(ChatRequestDto chatRequestDto)
	{
		return chatMessageService.sendSoloMessage(chatRequestDto);	 
	}
	
	
}
