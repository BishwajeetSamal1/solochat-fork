package com.example.solochat.dao;

import java.awt.print.Pageable;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.solochat.model.Users;

public interface UsersRepository extends JpaRepository<Users, Long>{
	@Query("SELECT u FROM Users u")
	List<Users> findAll(Pageable pageable);
}
