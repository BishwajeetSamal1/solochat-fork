package com.example.solochat.dao;

import java.awt.print.Pageable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.util.Streamable;

import com.example.solochat.model.ChatMessage;
import com.example.solochat.model.Users;

public interface ChatMessageRepository extends JpaRepository<ChatMessage, Long> {

//	Page<ChatMessage> findByFromUserAndToUserOrderByCreatedAt(Users users, Users users2,Pageable pageable);

	List<ChatMessage> findByFromUserAndToUserOrderByCreatedAt(Users users, Users users2);

}
