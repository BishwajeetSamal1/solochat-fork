package com.example.solochat.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.Data;






@Data
@Entity
public class ChatMessage extends IbSupport {
	@Id
	@GeneratedValue(generator = "message_id_generator")
	@SequenceGenerator(
			name = "massage_id_generator",
			sequenceName = "message_id_generator",
			initialValue = 1
			)
	private long id;
	private String content;
	@ManyToOne
	private Users fromUser;
	@ManyToOne
	private Users toUser;
	
}
