package com.example.solochat.model;

import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;

import lombok.Data;
@MappedSuperclass
@Data
public class IbSupport {
	private long createdAt = System.currentTimeMillis();
	private long deletedAt;
	private long updatedAt = System.currentTimeMillis();
	private boolean isDeleted;	
}
