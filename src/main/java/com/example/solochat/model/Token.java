package com.example.solochat.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Token {
	@Id
	@GeneratedValue(generator = "Token_id_generator")
	@SequenceGenerator(
			name = "Token_id_generator",
			sequenceName = "Token_id_generator",
			initialValue = 1
			)
	private long id;
	private String token;

	private String Users;
}
