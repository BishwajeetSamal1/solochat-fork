package com.example.solochat.dto;


import lombok.Data;

@Data
public class ChatRequestDto {
	
	private String content;
	
	private long toUser;
	
	private long fromUsers;

}
